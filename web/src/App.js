import React from 'react';
import FirebaseConfig from './FirebaseConfig';
import firebase from 'firebase';
firebase.initializeApp(FirebaseConfig);
import ReactFireMixin from 'reactfire';
import reactMixin from 'react-mixin';
import Toolbar from './Components/Toolbar';
import TodoList from './Components/TodoList';
import AddTodoForm from './Components/AddTodoForm';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Snackbar from 'material-ui/Snackbar';
import ContentAdd from 'material-ui/svg-icons/content/add';

class App extends React.Component {
    mixins:[ReactFireMixin]

    constructor(props){
        super(props);
        this.checkAuth();
        this.state = {
            todos: [],
            categories: [],
            completedTodos: [],
            isAddMode: false,
            isEditMode: false,
            editedTodo: {},
            loggedIn: false,
            user: {},
            isUpdatedMessageShown: false,
            isAddedMessageShown: false,
            isCompletedMessageShown: false
        };
    }

    componentWillMount(){
        let todosRef = firebase
            .database()
            .ref('todos');
        this.bindAsArray(todosRef, 'todos');

        let categoriesRef = firebase
            .database()
            .ref('categories');
        this.bindAsArray(categoriesRef, 'categories');

        let completedTodosRef = firebase
            .database()
            .ref('completed');
        this.bindAsArray(completedTodosRef, 'completedTodos')
    }

    toggleAddMode(){
        this.setState({
            isAddMode: !this.state.isAddMode
        });
    }

    toggleEditMode(){
        this.setState({
            isEditMode: !this.state.isEditMode
        });
    }

    submitTodo(todo){
        let ref = this
                .firebaseRefs['todos'],
            key = todo.key;
        delete todo.key;
        if (this.state.isEditMode) {
            ref.child(key).update(todo);
            this.setState({
                isEditMode: false,
                isAddMode: false
            });
        } else {
            ref.push(todo);
            this.toggleAddMode();
        }
    }

    completeTask(todo){
        this
            .firebaseRefs['todos']
            .child(todo['.key'])
            .remove();
        delete todo['.key'];
        this
            .firebaseRefs['completedTodos']
            .push(todo);
        this.setState({isCompletedMessageShown: true});
    }

    editTask(todo){
        this.setState({
            isEditMode: true,
            editedTodo: todo
        });
    }

    userLoggedIn(user){
        this.setState({
            loggedIn: true,
            user: user
        });
    }

    isUserLoggedIn(){
        return this.state.user.email === 'shashankan.10@gmail.com' || this.state.user.email === 'archana21090@gmail.com';
    }

    checkAuth(){
        let provider = new firebase.auth.GoogleAuthProvider();
        let self = this;
        firebase.auth().onAuthStateChanged(function (user){
            if (user) {
                // User is signed in.
                self.userLoggedIn(user);
            } else {
                // No user is signed in.
                firebase.auth().signInWithPopup(provider).then(function (result){
                    // The signed-in user info.
                    let user = result.user;
                    self.userLoggedIn(user);
                }).catch(function (error){
                    // The firebase.auth.AuthCredential type that was used.
                    self.setState({
                        loggedIn: false
                    });
                });
            }
        });
    }

    showHelp(){
    }

    showSettings(){
    }

    showCategoryList(){
    }

    render(){
        const fabStyle = {
            margin: 0,
            top: 'auto',
            right: 20,
            bottom: 20,
            left: 'auto',
            position: 'fixed',
            zIndex: 999999
        };

        return (
            <div>
                <div className="App">
                    {
                        !this.isUserLoggedIn() && <div>
                            You haven't logged in do not have sufficient privileges
                        </div>
                    }
                    {
                        this.isUserLoggedIn() && <Toolbar
                            title="Collados"
                            onAdd={this.toggleAddMode.bind(this)}
                            isAddMode={this.state.isAddMode || this.state.isEditMode}
                            showHelp={this.showHelp.bind(this)}
                            showSettings={this.showSettings.bind(this)}
                            showCategoryList={this.showCategoryList.bind(this)}
                        />
                    }
                    {
                        this.isUserLoggedIn() && !this.state.isAddMode && !this.state.isEditMode && <TodoList
                            todos={this.state.todos}
                            categories={this.state.categories}
                            completeTask={this.completeTask.bind(this)}
                            editTask={this.editTask.bind(this)}
                        />
                    }
                    {
                        this.isUserLoggedIn() && this.state.isAddMode && !this.state.isEditMode && <AddTodoForm
                            categories={this.state.categories}
                            submitTodo={this.submitTodo.bind(this)}
                        />
                    }
                    {
                        this.isUserLoggedIn() && !this.state.isAddMode && this.state.isEditMode && <AddTodoForm
                            categories={this.state.categories}
                            submitTodo={this.submitTodo.bind(this)}
                            isEditMode={this.state.isEditMode}
                            editedTodo={this.state.editedTodo}
                        />
                    }
                    {
                        !this.state.isAddMode && <FloatingActionButton secondary={true} style={fabStyle}
                                                                       onTouchTap={this.toggleAddMode.bind(this)}>
                            <ContentAdd />
                        </FloatingActionButton>
                    }
                </div>
                <Snackbar
                    open={this.state.isAddedMessageShown}
                    message='Added todo'
                    autoHideDuration={2000}
                />
                <Snackbar
                    open={this.state.isUpdatedMessageShown}
                    message='Added todo'
                    autoHideDuration={2000}
                />
                <Snackbar
                    open={this.state.isCompletedMessageShown}
                    message={'Marked complete'}
                    autoHideDuration={2000}
                />
            </div>
        );
    }
}
reactMixin(App.prototype, ReactFireMixin);

export default App;
