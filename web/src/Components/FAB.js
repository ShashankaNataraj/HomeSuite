import React from 'react';
import './FAB.css';
class FAB extends React.Component {
    render() {
        return <div className="fab">
            menu<i className="fa fa-add"></i>
        </div>
    }
}
export default FAB;