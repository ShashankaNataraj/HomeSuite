import React from 'react';
import './CategoryList.css';
import {List, ListItem} from 'material-ui/List';
import {green200} from 'material-ui/styles/colors';

//Icons from here on
import Home from 'material-ui/svg-icons/action/home';
import Drive from 'material-ui/svg-icons/notification/drive-eta';
import Exercise from 'material-ui/svg-icons/places/fitness-center';
import Financial from 'material-ui/svg-icons/action/receipt';
import HomeImprovement from 'material-ui/svg-icons/editor/format-paint';
import Internet from 'material-ui/svg-icons/hardware/router';
import School from 'material-ui/svg-icons/social/school';
import OpenSource from 'material-ui/svg-icons/device/developer-mode';
import Savings from 'material-ui/svg-icons/action/account-balance-wallet';
import ShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import TV from 'material-ui/svg-icons/hardware/tv';
import Navigation from 'material-ui/svg-icons/maps/navigation';
import Work from 'material-ui/svg-icons/action/work';
class CategoryList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selectedCategory: this.props.selectedCategory ? this.props.selectedCategory : ''
        };
    }

    getIconForCategory(category){
        const iconMap = {
            Apartment: <Home/>,
            Car: <Drive/>,
            Exercise: <Exercise/>,
            Financial: <Financial/>,
            'Home Improvement': <HomeImprovement/>,
            Internet: <Internet/>,
            Learn: <School/>,
            'Open source': <OpenSource/>,
            Savings: <Savings/>,
            Shopping: <ShoppingCart/>,
            Television: <TV/>,
            Visit: <Navigation/>,
            Work: <Work/>
        };
        return iconMap[category['.key']];
    }

    selectCategory(category){
        this.setState({selectedCategory: category['.key']});
        this
            .props
            .selectCategory(category['.key']);
    }

    render(){
        const selectedStyle = {
            backgroundColor: green200
        };
        const unselectedStyle = {};
        return <List>
            {
                this
                    .props
                    .categories
                    .map((category, idx) =>{
                        return <ListItem onTouchTap={this.selectCategory.bind(this, category)}
                                         key={idx}
                                         leftIcon={this.getIconForCategory((category))}
                                         primaryText={category['.key']}
                                         style={category['.key'] === this.state.selectedCategory ? selectedStyle : unselectedStyle}>
                        </ListItem>
                    })
            }
        </List>
    }
}

export default CategoryList;