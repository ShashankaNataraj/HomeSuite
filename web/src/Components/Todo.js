import React from 'react';
import {List, ListItem} from 'material-ui/List';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Moment from 'moment';
import {grey400, darkBlack,white} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Check from 'material-ui/svg-icons/navigation/check';
import Home from 'material-ui/svg-icons/action/home';
import Drive from 'material-ui/svg-icons/notification/drive-eta';
import Exercise from 'material-ui/svg-icons/places/fitness-center';
import Financial from 'material-ui/svg-icons/action/receipt';
import HomeImprovement from 'material-ui/svg-icons/editor/format-paint';
import Internet from 'material-ui/svg-icons/hardware/router';
import School from 'material-ui/svg-icons/social/school';
import OpenSource from 'material-ui/svg-icons/device/developer-mode';
import Savings from 'material-ui/svg-icons/action/account-balance-wallet';
import ShoppingCart from 'material-ui/svg-icons/action/shopping-cart';
import TV from 'material-ui/svg-icons/hardware/tv';
import Navigation from 'material-ui/svg-icons/maps/navigation';
import Work from 'material-ui/svg-icons/action/work';
import ModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';


class Todo extends React.Component {
    getIconForCategory(category){
        const iconMap = {
            Apartment: <Home/>,
            Car: <Drive/>,
            Exercise: <Exercise/>,
            Financial: <Financial/>,
            'Home Improvement': <HomeImprovement/>,
            Internet: <Internet/>,
            Learn: <School/>,
            'Open Source': <OpenSource/>,
            Savings: <Savings/>,
            Shopping: <ShoppingCart/>,
            Television: <TV/>,
            Visit: <Navigation/>,
            Work: <Work/>
        };
        return <Avatar backgroundColor={white}>{iconMap[category]}</Avatar>;
    }

    getDeadlineFromNow(){
        var deadline = Moment(this.props.todo.deadline)
            .endOf('day')
            .fromNow()
            .toString();
        return deadline.indexOf('Invalid') === -1 ? deadline : 'never';
    }

    isDeadlineInPast(){
        return this
                .getDeadlineFromNow()
                .indexOf('ago') > 0;
    }

    completeTask(){
        this.props.completeTask(this.props.todo);
    }

    editTask(){
        this.props.editTask(this.props.todo);
    }

    render(){
        const iconButtonElement = (
            <IconButton
                touch={true}
                tooltip="more"
                tooltipPosition="bottom-left">
                <MoreVertIcon color={grey400}/>
            </IconButton>
        );
        const todoItemStyle = {
            marginTop: -16
        };
        const menuStyle={
            marginTop:20
        };
        const rightIconMenu = (
            <IconMenu iconButtonElement={iconButtonElement} style={menuStyle}>
                <MenuItem leftIcon={<Check/>} onTouchTap={this.completeTask.bind(this)}>Mark Complete</MenuItem>
                <MenuItem leftIcon={<ModeEdit/>} onTouchTap={this.editTask.bind(this)}>Edit</MenuItem>
            </IconMenu>
        );
        return <List>
            <ListItem
                style={todoItemStyle}
                leftAvatar={this.getIconForCategory(this.props.todo.category)}
                rightIconButton={rightIconMenu}
                primaryText={this.props.todo.name}
                secondaryText={
                    <p>
                        <span style={{color: darkBlack}}>{this.props.todo.description}</span><br />
                        due {this.getDeadlineFromNow()}
                    </p>
                }
                secondaryTextLines={2}
            />
            <Divider/>
        </List>
    }
}
export default Todo;