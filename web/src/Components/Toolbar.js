import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/svg-icons/navigation/menu';
import Category from 'material-ui/svg-icons/action/book';
import Help from 'material-ui/svg-icons/action/help-outline';
import Settings from 'material-ui/svg-icons/action/settings';
import {teal900} from 'material-ui/styles/colors';
class Toolbar extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isDrawerOpen: false
        };
    }

    toggleDrawer(){
        this.setState({
            isDrawerOpen: !this.state.isDrawerOpen
        })
    }

    showCategoryList(){
        this.toggleDrawer();
        this.props.showCategoryList();
    }

    showHelp(){
        this.toggleDrawer();
        this.props.showHelp();
    }

    showSettings(){
        this.toggleDrawer();
        this.props.showSettings();
    }

    render(){
        const appBarStyle = {
            backgroundColor: teal900
        };
        return <div>
            <AppBar
                title={this.props.title}
                iconElementLeft={<IconButton onTouchTap={this.toggleDrawer.bind(this)}><Menu /></IconButton>}
                style={appBarStyle}
            />
            <Drawer open={this.state.isDrawerOpen}>
                <MenuItem onTouchTap={this.showCategoryList.bind(this)} leftIcon={<Category/>}>Configure
                    Categories</MenuItem>
                <MenuItem onTouchTap={this.showHelp.bind(this)} leftIcon={<Help/>}>Help</MenuItem>
                <Divider/>
                <MenuItem onTouchTap={this.showSettings.bind(this)} leftIcon={<Settings/>}>Settings</MenuItem>
            </Drawer>
        </div>
    }
}
export default Toolbar;