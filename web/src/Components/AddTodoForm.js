import React from 'react';
import CategoryList from './CategoryList';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import Moment from 'moment';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import Check from 'material-ui/svg-icons/navigation/check';


class AddTodoForm extends React.Component {
    constructor(props){
        super(props);
        let editMode = this.props.editedTodo;
        this.state = {
            name: editMode ? this.props.editedTodo.name : '',
            description: editMode ? this.props.editedTodo.description : '',
            deadline: editMode ? this.props.editedTodo.deadline : '',
            category: editMode ? this.props.editedTodo.category : '',
            key: editMode ? this.props.editedTodo['.key'] : ''
        };
    }

    setName(e){
        this.setState({
            name: e.target.value
        });
    }

    setDescription(e){
        this.setState({
            description: e.target.value
        });
    }

    setDeadline(selectedDate){
        this.setState({
            deadline: Moment(selectedDate).format('YYYY-MM-DD')
        });
    }

    setCategory(categoryName){
        this.setState({
            category: categoryName
        });
    }

    handleSubmit(){
        this.props.submitTodo(this.state);
    }

    render(){
        const style = {
            marginLeft: 20,
        };
        return <Paper zDepth={2}>
            <TextField
                hintText="Name"
                floatingLabelText="Name"
                onChange={this.setName.bind(this)}
                value={this.state.name}
                underlineShow={false}
                style={style}
            />
            <Divider />
            <TextField
                hintText="Description"
                floatingLabelText="Description"
                onChange={this.setDescription.bind(this)}
                value={this.state.description}
                underlineShow={false}
                style={style}
            />
            <Divider />
            <DatePicker hintText="Deadline"
                        onChange={(event, x) =>{
                            this.setDeadline(x)
                        }}
                        defaultDate={this.state.deadline && this.state.deadline !== '' ? new Date(this.state.deadline) : new Date()}
                        underlineShow={false}
                        style={style}/>
            <Divider />
            <CategoryList categories={this.props.categories}
                          selectCategory={this.setCategory.bind(this)}
                          selectedCategory={this.state.category}/><br />
            <RaisedButton label="Save"
                          fullWidth={true}
                          icon={<Check/>}
                          primary={true}
                          onTouchTap={this.handleSubmit.bind(this)}/>
        </Paper>;
    }
}

export default AddTodoForm;