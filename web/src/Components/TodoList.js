import React from 'react';
import Todo from './Todo';
import './TodoList.css';
class TodoList extends React.Component {
    constructor(props){
        super(props);
        this.state = {};
    }

    render(){
        const style = {
            marginTop: 8
        };
        return <div style={style}>
            {this
                .props
                .todos
                .map((todo, idx) =>{
                    return <Todo
                        key={todo['.key']}
                        todo={todo}
                        categoryIcon={this.props.categories[todo.category]}
                        categories={this.props.categories}
                        completeTask={this.props.completeTask}
                        editTask={this.props.editTask}
                    />
                })
            }

        </div>;
    }
}
export default TodoList;