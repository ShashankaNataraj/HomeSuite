import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './index.css';
import 'reset-css/reset.css';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

injectTapEventPlugin();

if ('serviceWorker' in navigator) {
    window.addEventListener('load', function (){
        navigator.serviceWorker.register('/sw.js').then(function (registration){
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }).catch(function (err){
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
        });
    });
}
ReactDOM.render(
    <MuiThemeProvider>
        <App />
    </MuiThemeProvider>,
    document.getElementById('root')
);
