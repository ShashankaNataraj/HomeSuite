Build Commands
--
* `yarn start` starts off a webpack dev server with live reload
* `yarn build` Builds the app and deploys to `build` directory
* `yarn deploy` builds and deploys the app on firebase hosting